module Main where

import Data.List (maximumBy)
import List ((\\))
import Ratio ((%), Rational)

type Memory = [(Integer, Integer)]
type Status = (Integer, Memory)

data Player = Larry Status | Robin Status

difference :: [Integer] -> Integer
difference calls = abs ((fst result) - (snd result))
    where
        result = game calls

average :: [Integer] -> Rational
average xs = (fst avg) % (snd avg)
    where
        avg = average' xs

        average' :: [Integer] -> (Integer, Integer)
        average' []       = error "empty list"
        average' [x]      = (x, 1)
        average' (x : xs) = addTuples (x, 1) (average' xs)

addTuples :: (Integer, Integer) -> (Integer, Integer) -> (Integer, Integer)
addTuples (x, y) (u, v) = (x + u, y + v)

game :: [Integer] -> (Integer, Integer)
game calls = turn calls (Larry (0, [])) (Robin (0, []))

turn :: [Integer] -> Player -> Player -> (Integer, Integer)
turn [] (Larry l) (Robin r)       = ((fst l), (fst r))
turn (x : xs) l r                 = turn xs (update l x) (update r x)

update :: Player -> Integer -> Player
update (Larry (s, m)) call | call `elem` (getStored m) = Larry ((s + 1), (calling call m))
                           | length m < 5              = Larry (s, (call, 0) : m)
                           | otherwise                 = Larry (s, (replace call m))
update (Robin (s, m)) call | call `elem` (getStored m) = Robin ((s + 1), m)
                           | length m < 5              = Robin (s, (call, 0) : (add m))
                           | otherwise                 = Robin (s, (replace call m))

getStored :: Memory -> [Integer]
getStored = map fst

calling :: Integer -> Memory -> Memory
calling n []                        = [(n, 0)]
calling n ((x, y) : ms) | n == x    = (x, 0) : ms
                        | otherwise = (x, (y + 1)) : (calling n ms)

replace :: Integer -> Memory -> Memory
replace n m = called \\ [(maximumBy compareSnd called)]
    where
        called = calling n m

add :: Memory -> Memory
add []            = []
add ((x, y) : ms) = (x, (y + 1)) : (add ms)

compareSnd :: Ord b => (a, b) -> (a, b) -> Ordering
compareSnd x y = compare (snd x) (snd y)
