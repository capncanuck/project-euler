module Main where

import Data.Numbers.Primes (primeFactors)

isSquareFree :: Integer -> Bool
isSquareFree = not . duplicates . primeFactors

duplicates :: [Integer] -> Bool
duplicates [] = False
duplicates (x : xs) | null u    = duplicates v
                    | otherwise = True
    where
        (u, v) = span (== x) xs

fib :: [Integer]
fib = [floor (phi ^ x / sqrt 5 + 0.5) | x <- [1 .. ]]

phi :: Floating a => a
phi = (1 + sqrt 5) / 2

squareFreeFib :: [Integer]
squareFreeFib = filter (isSquareFree) fib
