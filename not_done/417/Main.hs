module Main where

import Control.Monad (void)
import Control.Monad.Instances(Monad)
import Data.List (isPrefixOf, find, tails)
import Data.Maybe (isJust, fromJust)

import Prelude hiding (cycle)

divide :: Int -> Int -> [Int]
0 `divide` _ = []
x `divide` y = q : r `divide` y
    where
        (q, r) = (10 * x) `divMod` y

multiplicativeOrder :: Int -> Integer -> Int
multiplicativeOrder e n | 10 ^ e `mod` n == 1 = e
                        | otherwise           = multiplicativeOrder (e + 1) n

coveredBy :: [Int] -> [Int] -> Maybe [Int]
coveredBy xs = find (isPrefixOf xs . concat . repeat) . init . tails

cycle :: [Int] -> [Int] -> [Int]
cycle xs []                       = xs
cycle xs ys@(y : ys') | isJust t  = fromJust t
                      | otherwise = cycle (xs ++ [y]) ys'
    where
        t = ys `coveredBy` xs

l :: Int -> Int
l n | gcd n 10 == 1                  = (multiplicativeOrder 1 . fromIntegral) n
    | void xs > replicate (n - 1) () = (length . cycle [] . take (n - 1)) xs
    | otherwise                      = 0
    where
        xs = 1 `divide` n

main :: IO ()
main = (print . sum . map l) [3 .. 100000000]
