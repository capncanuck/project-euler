module Main where

import Data.Numbers.Primes (primeFactors)
import List (nub)

rad :: Integer -> Integer
rad = product . nub . primeFactors

abcHit :: Integer -> Integer -> Integer -> Bool
abcHit a b c  = gcd a b == 1
             && gcd a c == 1
             && gcd b c == 1
             && a < b
             && a + b == c
             && rad (a*b*c) < c
