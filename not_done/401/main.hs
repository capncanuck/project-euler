module Main where

import Data.Numbers.Primes (primeFactors)

sigma2 :: Integer -> Integer
sigma2 x = (sum . (1 :) . map (^ 2)) (x : primeFactors x)

bigSigma2 :: Integer -> Integer
bigSigma2 n = sum [sigma2 i | i <- [1 .. n]]
