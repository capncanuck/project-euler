module Main where

import qualified Data.Bits as B (xor)

buildKey :: String -> String -> String
buildKey key = buildKey' key key
    where
        buildKey' :: String -> String -> String -> String
        buildKey' key ys []             = []
        buildKey' key [] xs             = buildKey' key key xs
        buildKey' key (x : xs) (y : ys) = x : buildKey' key xs ys

xor :: Char -> Char -> Char
x `xor` y = toEnum (fromEnum x `B.xor` fromEnum y)

crypt :: String -> String -> String
crypt xs ys = zipWith xor xs (buildKey ys xs)

main :: IO ()
main = do
    file <- readFile "cipher1.txt"
    mapM_ putStrLn (map (crypt (map toEnum (read ("[" ++ file ++ "]")))) [x : y : z : [] | x <- ['a' .. 'z'], y <- ['a' .. 'z'], z <- ['a' .. 'z']])
