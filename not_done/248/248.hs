module Main where

import Control.Arrow ((&&&))
import Data.List (group)
import Data.Numbers.Primes (primeFactors)

-- Totient Function
phi :: Integer -> Integer
phi n = product [(p - 1) * p ^ (m - 1) | (p, m) <- primeFactorsMult n]

primeFactorsMult :: Integer -> [(Integer, Int)]
primeFactorsMult = map (head &&& length) . group . primeFactors

-- Factorial Function
fac :: Int -> Integer
fac = (!!) (scanl (*) 1 [1 .. ])

-- Table of n
-- 6227180929
-- 6227182993
-- 6227186509
