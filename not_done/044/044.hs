module Main where

pentagonal :: Integral a => a -> Bool
pentagonal n = n == (p . ip) n
    where
        p :: Integral a => a -> a
        p n = n * (3 * n - 1) `div` 2

        ip :: Integral a => a -> a
        ip n = truncate $ flip (/) 6 $ 1 + sqrt (1 + (24 * fromIntegral n))
