module Main where

{-u :: Fractional a => a -> a
u 0 = -1
u n = (f . u) (n - 1)
-}

f :: Floating a => a -> a
f n = fromInteger (floor (2 ** (30.403243784 * n^2))) * 10e-9
