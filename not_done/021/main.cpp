/* Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.
 *
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284.
 * The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 *
 * Evaluate the sum of all the amicable numbers under 10000.
 *
 * Solution: 31626
 */

#include <iostream>
#include <cmath>
using namespace std;

int SumOfProperDivisors(int num) {
    if (num == 1) {
        return 0;
    } else {
        int limit = floor(sqrt(num));
        int sum;

        if (pow(limit, 2) == num) {
            sum = limit + 1;
            limit--;
        } else {
            sum = 1;
        }

        int test;
        int i;

        if (num % 2 == 1) {
            test = 3;
            i = 2;
        } else {
            test = 2;
            i = 1;
        }

        while (test < limit + 1) {
            if (num % test == 0) {
                sum += test + (num / test);
            }

            test += i;
        }

        return sum;
    }
}

int main() {
    int sum = 0;
    int b;

    for (int a = 2; a < 1e4; a++) {
        b = SumOfProperDivisors(a);
        if (b > a && SumOfProperDivisors(b) == a) {
            cout << a << "\t" << b << endl;
            sum += a + b;
        }
    }

    cout << endl << sum << endl;

    return 0;
}
