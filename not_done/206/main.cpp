/* Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
 * where each “_” is a single digit.
 *
 * Number is between 1010101010 and 1389026623
 */

#include <iostream>
using namespace std;

int main() {
    //int upper = 1389026623;
    //int lower = 1010101010;

    int array[20];
    array[19] = 0;

    for (int i = 2; i < 20; i += 2) {
        array[i] = i/2;
        //cout << array[i] << endl;
    }

    //cout << array[19] << endl;

    return 0;
}
