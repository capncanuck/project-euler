module Main where

import List (nub)

distinctPart :: Int -> [[Int]]
distinctPart 0             = [[]]
distinctPart n | n < 0     = []
               | otherwise = part' n 1 where
    part' :: Int -> Int -> [[Int]]
    part' m k | m < k     = []
              | m <= 2*k  = [[m]]
              | otherwise = map (k :) (part' (m - k) (k + 1)) ++ part' m (k + 1)

f :: Int -> Int
f = \ n -> maximum $ map product $ distinctPart n

m :: Int -> Int
m = \ n -> length $ filter (\ x -> product x == f n) (distinctPart n) !! 0

fm :: Int -> Int
fm = \ n -> (f n)*(m n)
