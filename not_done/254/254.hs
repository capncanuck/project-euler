module Main where

fac :: Integral a => a -> a
fac = product . enumFromTo 1

digitSum :: Integer -> Integer
digitSum = (sum . map (read . return) . show)

f :: Integer -> Integer
f = (sum . map (fac . read . return) . show)

sf :: Integer -> Integer
sf = (digitSum . f)

g :: Integer -> Integer
g i = [n | n <- [1 .. ], sf n == i] !! 0

sg :: Integer -> Integer
sg = (digitSum . g)

main :: IO ()
main = print $ (sum . (map sg)) [1 .. 150]
