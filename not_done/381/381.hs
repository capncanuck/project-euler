module Main where

import Data.Numbers.Primes (primes)

primeRange :: Integer -> [Integer]
primeRange p = takeWhile (< p) (drop 2 primes)

factorial :: Integer -> Integer
factorial = (map fac [0 .. ] !!) . fromIntegral

fac :: Integer -> Integer
fac = product . enumFromTo 1

m_subS :: Integer -> Integer
m_subS = (map subS [0 .. ] !!) . fromIntegral

subS :: Integer -> Integer
subS n = sum [factorial (n - k) | k <- [1 .. 5]]

s :: Integer -> Integer
s p = (subS p) `rem` p

sumS :: Integer -> Integer
sumS p = sum [s n | n <- (primeRange p)]

main :: IO ()
main = print $ sumS (10^8)
