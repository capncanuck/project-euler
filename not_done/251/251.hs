module Main where

cardanoTriplet :: (Floating a, Ord a) => (a, a, a) -> a
cardanoTriplet (a, b, c) = crt (a + b * sqrt c) + crt (a - b * sqrt c)

crt :: (Floating a, Ord a) => a -> a
crt x = case x of
    _ | x < 0     -> - ((- x) ** (1 / 3))
      | otherwise -> x ** (1 / 3)
