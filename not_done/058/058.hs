module Main where

import Data.Numbers.Primes (isPrime)
import Ratio (Ratio, (%))

diagonals :: Integral a => a -> [a]
diagonals m = [x^2 + (x + 1) `rem` 2 | x <- [2, 4 .. m]] ++ [x^2 + x + 1 | x <- [1 .. (m - 1)]]

primeDiagonals :: Integral a => a -> [a]
primeDiagonals = (filter isPrime) . diagonals

primeRatio :: Integral a => a -> Ratio a
primeRatio n = (fromIntegral . length . primeDiagonals) n % (spiralSize n)

spiralSize :: Integral a => a -> a
spiralSize n = 2 * n - 1

main :: IO ()
main =
    print $
        fst $
            flip (!!) 0 $
                filter (\ (x, y) -> y < 1 % 10) $
                    [(x, primeRatio x) | x <- [25000 .. ]] -- 25000 is where < 5041 % 49999
