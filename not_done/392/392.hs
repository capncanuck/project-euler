module Main where

import Data.Ratio

type Coord = (Rational, Rational)

area :: [Coord] -> Rational
area = (4 *) . sum . map (curry *)

lines :: Int -> [Coord]
lines = optimal [(1, 1)]

optimal :: [Coord] -> Int -> [Coord]
optimal cs 0 = cs
optimal cs n =
