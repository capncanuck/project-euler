module Main where

r :: Integral a => a -> a -> a
r a n = rem ((a - 1)^n + (a + 1)^n) (a^2)

rMax :: Integral a => a -> a
rMax a = maximum [r a n | n <- [1, 3 .. (a^2)]]

main :: IO ()
main = print $ sum [rMax a | a <- [3 .. 1000]]
