module Main where

import Data.Ratio ((%), numerator)

lowerLimit = 1 % 3
upperLimit = 1 % 2

getLimits :: Int -> (Int, Int)
getLimits x | low == lowerLimit = (ceiling low + 1, floor high)
              otherwise         = (ceiling low, floor high)
    where
        low  = (x % 1 * lowerLimit)
        high = (x % 1 * upperLimit)

        (low, high) = ()
