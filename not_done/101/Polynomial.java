import Jama.Matrix;

public class Polynomial {
    private double[] points;
    private double[] polynomial;

    public Polynomial(double[] points) {
        this.points = points;
        this.polynomial = getPolynomial(vstar(this.points));
    }

    public double eval() {
        double x = (double) points.length + 1;
        double output = 0;

        for (int i = 0; i < points.length + 1; i++) {
            output += this.polynomial[i] * Math.pow(x, i);
        }

        return output;
    }

    private double[] getPolynomial(Matrix vstar) {
        return vstar.transpose().getArray()[0];
    }

    private Matrix vstar(double[] points) {
        Matrix y = new Matrix(getY(points));
        Matrix M = new Matrix(getM(points));

        return M.inverse().times(M).inverse().times(M.inverse()).times(y);
    }

    private double[][] getM(double[] points) {
        int n = points.length;
        double[][] M = new double[n][n + 1];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n + 1; j++) {
                M[i][j] = Math.pow((points[i]), j);
            }
        }

        return M;
    }

    private double[][] getY(double[] points) {
        double[][] y = new double[points.length][0];

        for (int i = 0; i < points.length; i++) {
            y[i][0] = points[i];
        }

        return y;
    }
}
