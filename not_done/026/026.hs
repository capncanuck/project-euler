module Main where

divide :: Integral a => a -> a -> String
divide 0 _             = []
divide x y | x > y     = show (x `div` y) ++ divide (x `rem` y) y
           | x == y    = "1"
           | otherwise = carry x y

carry x y | s < y      = '0' : divide s y
          | otherwise  = divide s y
    where
        s = 10 * x

cycler :: Integral a => a -> a -> [a]
cycler x y | x < y     = cycler (10 * x) y
             | otherwise = r : cycler r y
    where
        r = x `mod` y
