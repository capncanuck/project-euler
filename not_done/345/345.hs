module Main where

data Row a = List [a]
data Matrix a = Row[Row a]

popMaxMatrix :: Matrix Integer -> (Integer, Matrix Integer)
popMaxMatrix matrix = (maxElem, removeStarWith maxElem maxtrix) where
    maxElem = maximum (maxMatrix matrix)

    maxMatrix :: Matrix Integer -> Row Integer
    maxMatrix (x : xs) = maximum x : maxMatrix xs

    removeStarWith :: Integer -> Matrix Integer -> Matrix Integer
    removeStarWith x matrix =
