module Main where

import Data.Char (digitToInt)

sumDigits :: String -> Int
sumDigits = sum . (map digitToInt)

digitalRoots :: Int -> [Int]
digitalRoots n | length shown == 1 = [n]
               | otherwise         = n : (digitalRoots . sumDigits) shown
    where
        shown = show n

segments :: Int -> Int
segments n | n > 9     = segments (n `rem` 10) + segments (n `div` 10)
           | otherwise = case n of
                0 -> 6
                1 -> 2
                2 -> 5
                3 -> 5
                4 -> 4
                5 -> 5
                6 -> 6
                7 -> 4
                8 -> 7
                9 -> 6

samTransitions :: Int -> Int
samTransitions n = sum $ map ((* 2) . segments) (digitalRoots n)

deltaTransition :: Maybe Int -> Maybe Int -> Int
deltaTransition Nothing y                          = segments y
deltaTransition x Nothing                          = segments x
deltaTransition Nothing Nothing                    = 0
deltaTransition (Just x) (Just y) | x > 9 && y > 9 = transition (x `rem` 10) (y `rem` 10) + deltaTransition (x `div` 10) (y `div` 10)
                                  | x > 9          = transition (x `rem` 10) y + deltaTransition (x `div` 10) y
                                  | y > 9          = transition x (y `rem` 10) + deltaTransition x (y `div` 10)
                                  | otherwise      = transition x y
    where
        transition :: Int -> Int -> Int
        transition 8 y             = 7 - segments y
        transition x 8             = 7 - segments x
        transition x y | x == y    = 0
                       | x > y     = transition y x
                       | otherwise = case (x, y) of
                            (0, 1) -> 4
                            (0, 2) -> 3
                            (0, 3) -> 3
                            (0, 4) -> 4
                            (0, 5) -> 3
                            (0, 6) -> 2
                            (0, 7) -> 2
                            (1, 2) -> 5
