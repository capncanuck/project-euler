module Main (main) where

hypex :: Integer -> Integer -> Integer
a `hypex` 1 = a
a `hypex` k = a ^ (a `hypex` (k - 1))

main :: IO ()
main = print $ (1777 `hypex` 1855) `rem` 100000000
