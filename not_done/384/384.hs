module Main where

import Data.List (findIndices)
import Prelude hiding ((!!))

toBinary :: Integer -> [Int]
toBinary 0 = [0]
toBinary 1 = [1]
toBinary n = toBinary q ++ [fromInteger r]
    where
        (q, r) = quotRem n 2

toDecimal :: [Integer] -> Integer
toDecimal = sum . map (2^) . findIndices (== 1) . reverse

-- https://oeis.org/A004780
a :: Integer -> Integer
a n = (a' . toBinary) n
    where
        a' :: [Int] -> Integer
        a' []                 = 0
        a' [x]                = 0
        a' (_ : 0 : rest)     = a' rest
        a' (0 : rest@(1 : _)) = a' rest
        a' (1 : rest@(1 : _)) = 1 + a' rest

        {-a' x | n < 2     = 0
             | otherwise = a' first + a' second + splitAdjacent (last first) (head second)
            where
                n      = length x
                split  = splitAt (div n 2) x
                first  = fst split
                second = snd split

                splitAdjacent :: Int -> Int -> Integer
                splitAdjacent 1 1 = 1
                splitAdjacent x y = 0-}

b :: Integer -> Integer
b n | (even . a) n = 1
    | otherwise    = -1

bs :: [Integer]
bs = [b i | i <- [0 ..]]

ss :: [Integer]
ss = scanl1 (+) bs

s :: Integer -> Integer
s = (ss !!)

g :: Integer -> Integer -> Integer
g = g' ss
    where
        g' :: [Integer] -> Integer -> Integer -> Integer
        g' _ _ 0                    = -1
        g' (x : xs) t c | x == t    = 1 + g' xs t (c - 1)
                        | otherwise = 1 + g' xs t c

(!!) :: [a] -> Integer -> a
(x : _)  !! 0  = x
(_ : xs) !! n  = xs !! (n - 1)

fib :: Integer -> Integer
fib = (fib' !!)
    where
        fib' :: [Integer]
        fib' = 1 : 1 : zipWith (+) fib' (tail fib')

gf :: Integer -> Integer
gf t = g (fib t) (fib (t - 1))

main :: IO ()
main = print $ sum [gf t | t <- [2 .. 45]]
