module Main where

import Ratio (Ratio, denominator)

data Shape a = Triangle (a, a, a) deriving Show

areaIntegral :: Integral a => Shape (Ratio a) -> Bool
areaIntegral (Triangle (a, b, c)) = denominator squareArea == 1
    where
        squareArea = s*(s - a)*(s - b)*(s - c)

        s = (a + b + c) / 2

perimeter :: Num a => Shape a -> a
perimeter (Triangle xs) = (sum . curry3) xs
    where
        curry3 :: (a, a, a) -> [a]
        curry3 (x, y, z) = [x, y, z]

{-      Actually, a triangle with valid sides has 2 equal lengths that are odd
    with the third side +/- the other lengths.

    Let x = [1, 3 .. ]. (length of each equal side)
    Let y1 = 2x + (x + 1) = 3x + 1 < 10^9
    Let y2 = 2x + (x - 1) = 3x - 1 < 10^9

    y1 + y2 = 6x < 2*10^9

    The third side can not equal 0, occurs for Triangle (1, 1, 0). So subtract one.

    sum of perimeters = sum [6, 18 .. (2*10^9 - 2)] - 1
                      = 166666665333333335
-}
