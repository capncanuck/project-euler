module Main where

import Data.Packed.Matrix (Matrix)
import Data.Packed.Vector (Vector)
import Numeric.LinearAlgebra.Algorithms (inv)
import Numeric.LinearAlgebra.LAPACK (multiplyF)
import Data.List (transpose)

un :: Int -> Int
un n = 1 - n + n^2 - n^3 + n^4 - n^5 + n^6 - n^7 + n^8 - n^9 + n^10

vn :: Int -> Int
vn n = n^3

op :: (Int -> Int) -> Int -> Int -> Int
op f n = polyBestFit [f x | x <- [1 .. n]]

polyBestFit :: [Int] -> Int -> Int
polyBestFit v = (v2poly . vstar) (Vector v)

vstar :: Vector Int -> Vector Int
vstar v = (inv (mt `multiplyF` m) `multiply` mt) `multiplyF` v
    where
        mt = transpose m
        m = createM (length v)

        createM :: Int -> Matrix Int
