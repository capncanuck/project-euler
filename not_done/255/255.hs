module Main where

import Ratio ((%), Ratio)

digitCount :: Integral a => a -> Int
digitCount = length . show

roundedSqrtSteps :: Integral a => a -> a
roundedSqrtSteps n = loop 0 (-1) x0
    where
        x0 :: Integral a => a
        x0 | odd d     = 2 * 10^(div (d - 1) 2)
           | otherwise = 7 * 10^(div (d - 2) 2)

        d = digitCount n

        loop steps prev cur | prev == cur = steps
                            | otherwise   = loop (steps + 1) cur (next cur)

        next x = floor ((x + ceiling (n % x)) % 2)

average :: Integral a => [a] -> Ratio a
average xs = (fst avg) % (snd avg)
    where
        avg = average' xs

        average' :: Integral a => [a] -> (a, a)
        average' []       = error "empty list"
        average' [x]      = (x, 1)
        average' (x : xs) = addTuples (x, 1) (average' xs)

        addTuples :: Integral a => (a, a) -> (a, a) -> (a, a)
        addTuples (x, y) (u, v) = (x + u, y + v)

main :: IO ()
main =
    print $
        let n = 14 in
            average [roundedSqrtSteps x | x <- [10^(n - 1) .. 10^n - 1]]
