module Main where

import Complex (Complex ((:+)), realPart, imagPart)

isGaussian :: RealFloat a => Complex a -> Bool
isGaussian c = (check . realPart) c && (check . imagPart) c
    where
        check :: RealFloat a => a -> Bool
        check x = x == (fromIntegral . floor) x
