x = [1 1 -4/3+4/3*(sqrt(2)) 0];
y = [0 -4/3+4/3*(sqrt(2)) 1 1];

plot(x,y);
hold on

x1 = [1 (-4/3+4/3*(sqrt(2))+1)/2 (-4/3+4/3*(sqrt(2)))/2];
y1 = [(-4/3+4/3*(sqrt(2)))/2 (-4/3+4/3*(sqrt(2))+1)/2 1];

plot(x1,y1);
hold on

x2 = [(-4/3+4/3*(sqrt(2))+3)/4 (2*(-4/3+4/3*(sqrt(2)))+1)/4];
y2 = x2(:,[2 1]);

plot(x2,y2);
hold on

bx = [0 sqrt(2)/2];
by = [0 sqrt(2)/2];

scatter(bx,by);
hold on

[x3,y3,z3] = cylinder(1,200);

plot(x3(1,:),y3(1,:));
axis equal
grid on
