module Main where

isBalanced :: Integer -> Bool
isBalanced n | sum x == sum y = True
             | otherwise      = False
    where
        toDigits = (map (read . (:[])) . show) n
        len      = (fromIntegral . length) toDigits
        (x, y)   = balance (ceiling (len / 2)) toDigits

balance :: Int -> [Int] -> ([Int], [Int])
balance n xs = (take n xs, (take n . reverse) xs)

t :: Int -> Integer
t n = (sum . filter isBalanced) [1 .. 10 ^ n - 1]
