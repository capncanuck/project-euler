module Main (main) where

r ::  Integer -> Integer -> Int
r m n = length [(x, y) | x <- [(m + 1) .. n], y <- [(m + 1) .. n], odd (y^2 `div` x^2)]

main :: IO ()
main = print $ r (2*10^6) (10^9)
