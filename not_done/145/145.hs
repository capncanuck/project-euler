module Main where

import Data.Char (digitToInt)

revSum :: Integer -> Integer
revSum n | rshown !! 0 == '0' = 0
         | otherwise          = n + read rshown
    where
        rshown = (reverse . show) n

reversible :: Integer -> Bool
reversible n | even revSumn = False
             | otherwise    = and $ map (odd . digitToInt) $ show revSumn
    where
        revSumn = revSum n
