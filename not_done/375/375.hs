module Main where

import Data.Numbers.Primes (isPrime)

divisors :: Integral a => a -> [a]
divisors n = concat [[d, n `quot` d] | d <- [1 .. k], d `divides` n]
    where
        k = floor (sqrt (fromIntegral n))

        divides :: Integral a => a -> a -> Bool
        d `divides` n | d == 0    = error "divides: zero divisor"
                      | otherwise = n `rem` d == 0

checkDivisors :: Integral a => a -> Bool
checkDivisors n = ((check n) . divisors) n
    where
        check :: Integral a => a -> [a] -> Bool
        check _ []       = True
        check n (x : xs) = isPrime (x + n `div` x) && check n xs

main :: IO ()
main =
    print
        $ sum
        $ filter checkDivisors
        $ 1 : [2, 4 .. (100000000 - 1)]
