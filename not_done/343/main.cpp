/* For any positive integer k, a finite sequence ai of fractions xi/yi is defined by:
 * a1 = 1/k and
 * ai = (xi-1+1)/(yi-1-1) reduced to lowest terms for i>1.
 * When ai reaches some integer n, the sequence stops. (That is, when yi=1.)
 * Define f(k) = n.
 * For example, for k = 20:
 *
 * 1/20 → 2/19 → 3/18 = 1/6 → 2/5 → 3/4 → 4/3 → 5/2 → 6/1 = 6
 *
 * So f(20) = 6.
 *
 * Also f(1) = 1, f(2) = 2, f(3) = 1 and Σf(k^3) = 118937 for 1 ≤ k ≤ 100.
 *
 * Find Σf(k^3) for 1 ≤ k ≤ 2×10^6.
 */

//#define DEBUG
//#define LOG

#ifdef LOG
    #include <stdio.h>
#endif

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <map>
#include <vector>

using namespace std;
typedef long long ll;
typedef unsigned long long ull;

int main(int argc, char* argv[]) {
    int LIMIT = 2e6;
    ll sum = 0;
    ll K;
    ll fraction[2];
    ll current = 1;
    map<ll, ll> m;
    vector<ll> repeated;

    if (argc == 2) {
        LIMIT = atoi(argv[1]);
    }

    for (int k = 1; k < LIMIT + 1; k++) {

        #ifdef DEBUG
            if (!(k % 1)) {
                cout << k << endl;
            }
        #endif

        #ifdef LOG
            if (!(k % 1000)) {
                cout << k << endl;
            }

            // Clears the stdout buffer so output can be redirected to a file line-by-line
            // ./a.out > log
            setvbuf(stdout, NULL, _IOLBF, NULL);
        #endif

        K = pow(k, 3);
        fraction[0] = 1;
        fraction[1] = K;

        while (true) {

            #ifdef DEBUG
                cout << '\t' << fraction[0] << '/' << fraction[1] << endl;
            #endif

            if (fraction[0] == 1) {

                current = fraction[1];

                if (!(m.count(fraction[1]))) {
                    repeated.push_back(fraction[1]);
                } else {

                    sum += m[fraction[1]];

                    #ifdef DEBUG
                        cout << "shortcut: " << fraction[1] << " -> " << m[fraction[1]] << endl;
                        cout << '\t' << "+ " << m[fraction[1]] << " = " << sum << endl;
                    #endif

                    break;
                }
            }

            if (fraction[0] > fraction[1]) {

                m[current] = current;
                sum += current;

                #ifdef DEBUG
                    cout << "By symmetry, there will be no more reductions for " << current << endl;
                    cout << '\t' << "+ " << current << " = " << sum << endl;
                #endif

                for (ull i = 0; i < repeated.size(); i++) {
                    m[repeated[i]] = current;
                }

                while (!repeated.empty()) {
                    repeated.pop_back();
                }

                break;
            }

            if (fraction[1] != 1) {
                fraction[0]++;
                fraction[1]--;
            } else {
                sum += fraction[0];

                #ifdef DEBUG
                    cout << '\t' << "+ " << fraction[0] << " = " << sum << endl;
                #endif

                for (ull i = 0; i < repeated.size(); i++) {
                    m[repeated[i]] = fraction[0];
                }

                while (!repeated.empty()) {
                    repeated.pop_back();
                }

                break;
            }

            if (!(fraction[1] % fraction[0])) {

                #ifdef DEBUG
                    cout << "\t\t" << fraction[0] << '/' << fraction[1] << endl;
                #endif

                fraction[1] /= fraction[0];
                fraction[0] = 1;
            }
        }
    }

    cout << sum << endl;

    return 0;
}
