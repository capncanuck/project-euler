module Main where

import List (find)

a :: Integer -> [(Integer, Char)]
a 1 = []
a n =
    case n `rem` 3 of
        0 -> (n, 'D') : a (div n         3)
        1 -> (n, 'U') : a (div (4*n + 2) 3)
        2 -> (n, 'd') : a (div (2*n - 1) 3)

main :: IO ()
main = print $ find (\ n -> (take 30 $ map snd (a n)) == "UDDDUdddDDUDDddDdDddDDUDDdUUDd") [10^15 + 1 .. ]
