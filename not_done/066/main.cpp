/*Consider quadratic Diophantine equations of the form:
 *
 * x^2 – Dy^2 = 1
 *
 * For example, when D=13, the minimal solution in x is 649^2 – 13×180^2 = 1.
 *
 * It can be assumed that there are no solutions in positive integers when D is square.
 *
 * By finding minimal solutions in x for D = {2, 3, 5, 6, 7}, we obtain the following:
 *
 * 3^2 – 2×2^2 = 1
 * 2^2 – 3×1^2 = 1
 * 9^2 – 5×4^2 = 1
 * 5^2 – 6×2^2 = 1
 * 8^2 – 7×3^2 = 1
 *
 * Hence, by considering minimal solutions in x for D ≤ 7, the largest x is obtained when D=5.
 *
 * Find the value of D ≤ 1000 in minimal solutions of x for which the largest value of x is obtained.
 */

#include <iostream>
#include <cmath>

using namespace std;
typedef long long ll;

class diophantine {
    public:
        ll x;
        int D;
        ll y;
};

int main() {
    int D = 1;
    ll x, y;

    diophantine optimal;

    optimal.x = 2;
    optimal.D = 0;
    optimal.y = 1;

    while (D < 101) {

        D++;

        if (sqrt(D) == (int) sqrt(D)) {
            D++;
        }

        y = 1;
        cout << D << endl;

        while (true) {
            if (sqrt(1 + D*pow(y, 2)) == (int) sqrt(1 + D*pow(y, 2))) {
                x = (int) sqrt(1 + D*pow(y, 2));
                break;
            } else {
                y++;
            }
        }

        if (x > optimal.x) {
            optimal.x = x;
            optimal.D = D;
            optimal.y = y;
        }
    }

    cout << optimal.x << "^2 - " << optimal.D << "*" << optimal.y << "^2 = 1" << endl;

    return 0;
}
