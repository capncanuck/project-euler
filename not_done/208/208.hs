module Main where

data Direction = CW | CCW
    deriving Show

type Path = [Direction]

arcs :: Int -> [Path] -> [Path]
arcs 0 p = p
arcs n p = (arcs (n - 1) . addDirection) p

addDirection :: [Path] -> [Path]
addDirection []       = []
addDirection [[]]     = [[CCW], [CW]]
addDirection (x : xs) = [x ++ [CW]] ++ [x ++ [CCW]] ++ addDirection xs

filterLoops :: [Path] -> [Path]
filterLoops =
