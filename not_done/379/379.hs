module Main where

import CombinatoricsGeneration (combinationsOf)

divisors :: Integer -> [Integer]
divisors n = filter ((== 0) . rem n) [2 .. n `div` 2]

lcmArray :: [Integer] -> Integer
lcmArray [x, y] = lcm x y

f :: Integer -> Int
f n = (+) 1 $ length $ filter ((== n) . lcmArray) $ combinationsOf 2 (divisors n)

g :: Integer -> Int
g n = sum [f i | i <- [1 .. n]]
