module Main where

import Data.Numbers.Primes (primes)

s :: [Int]
s = takeWhile (< 5000) primes

s' :: [Int]
s' = tail s

subsequences :: [a] -> [[a]]
subsequences []       = []
subsequences (x : xs) = [x] : foldr f [] (subsequences xs)
    where
        f ys r = ys : (x : ys) : r
