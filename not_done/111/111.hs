module Main where

import Data.Numbers.Primes (isPrime)

primesOfDigit :: Integral a => a -> [a]
primesOfDigit n = [x | x <- [10^(n-1) .. 10^n-1], isPrime x]
