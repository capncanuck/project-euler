module Main where

divides :: Integral a => a -> a -> Bool
d `divides` n | d == 0    = error "divides: zero divisor"
              | otherwise = n `rem` d == 0

fac :: Integer -> Integer
fac = product . enumFromTo 1

comb :: Integer -> Integer -> Integer
comb n r | n == r                           = 1
         | r == 1                           = n
         | r > n                            = 0
         | r > n `div` 2                    = comb n (n - r)
         | r > 0 && (n - r + 1) `divides` r = (div (n - r + 1) (r)) * comb n (r - 1)
         | r < n && n `divides` (n - r)     = (div n (n - r)) * comb (n - 1) r
         {-| r < n                            = comb (n - 1) (r - 1) + comb (n - 1) r -}
         | n > 0 && r > 0 && n `divides` r  = (div n r) * comb (n - 1) (r - 1)
         | otherwise                        = div (fac n) $ (fac r) * fac (n - r)

t :: Integer -> Integer -> Int
t m n = length $ filter (10 `divides`) [comb i n | i <- [n .. m]]

main :: IO ()
main = print $ t (10^9) (10^7-10)
