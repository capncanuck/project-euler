module Main where

preTeens =
    [
        4, -- ZERO
        3, -- ONE
        3, -- TWO
        5, -- THREE
        4, -- FOUR
        4, -- FIVE
        3, -- SIX
        5, -- SEVEN
        5, -- EIGHT
        4, -- NINE
        3, -- TEN
        6, -- ELEVEN
        6, -- TWELVE
        8  -- THIRTEEN
    ]

tens =
    [
        0, -- NULL
        0, -- NULL
        6, -- TWENTY
        6, -- THIRTY
        5, -- FORTY
        5, -- FIFTY
        5, -- SIXTY
        7, -- SEVENTY
        6, -- EIGHTY
        6  -- NINETY
    ]

numOfLetters :: Int -> Int
numOfLetters n | n < 14                       = preTeens !! n
               | n < 20                       = teens n
               | n < 100 && n `rem` 10 == 0   = tens !! (n `div` 10)
               | n < 100                      = tens !! (n `div` 10) + preTeens !! (n `rem` 10)
               | n < 1000 && n `rem` 100 == 0 = preTeens !! (n `div` 100) + 7                               -- HUNDRED
               | n < 1000                     = preTeens !! (n `div` 100) + 10 + numOfLetters (n `rem` 100) -- HUNDRED AND
               | n == 1000                    = 11                                                          -- ONE THOUSAND


teens :: Int -> Int
teens n = case n of
    15        -> 7                            -- FIFTEEN
    18        -> 8                            -- EIGHTEEN
    otherwise -> preTeens !! (n `rem` 10) + 4 -- TEEN
