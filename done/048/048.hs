module Main (main) where

main :: IO ()
main = print $ rem (sum [x^x | x <- [1 .. 1000]]) 10000000000
