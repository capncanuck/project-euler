module Main (main) where

fac :: Integer -> Integer
fac = product . enumFromTo 1

comb :: Integer -> Integer -> Integer
comb n r = div (fac n) $ fac r * fac (n - r)

main :: IO ()
main = print $ length $ filter (> 1000000) [comb n r | n <- [1 .. 100], r <- [1 .. n]]
