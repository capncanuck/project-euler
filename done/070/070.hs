module Main where

import Data.List (minimumBy, nub, (\\))
import Data.Numbers.Primes (primeFactors)
import Data.Ratio (Ratio, (%), numerator)

phi :: Integral a => a -> a
phi n = numerator $ (n % 1) * productF ((nub . primeFactors) n)
    where
        productF :: Integral a => [a] -> Ratio a
        productF [] = 1
        productF [x] = 1 - (1 / fromIntegral x)
        productF (x : xs) = productF [x] * productF xs

intPerm :: Integral a => a -> a -> Bool
intPerm x y | x == y                 = False
            | length sx /= length sy = False
            | otherwise              = isPerm sx sy
    where
        sx = show x
        sy = show y

        isPerm :: Eq a => [a] -> [a] -> Bool
        isPerm [] [] = True
        isPerm [] x = False
        isPerm (x : xs) ys | x `elem` ys = isPerm xs (ys \\ [x])
                           | otherwise   = False

main :: IO ()
main = print
     $ fst
     $ minimumBy compareSnd
     $ (783169, 783169 % 781396) : [(x, x % y) | x <- [10^6 .. 10^7], let y = phi x, intPerm x y]
        where
            compareSnd :: (Ord a, Ord b) => (a, b) -> (a, b) -> Ordering
            compareSnd x y = compare (snd x) (snd y)
