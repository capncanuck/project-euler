module Main where

import Data.List (nub)

divides :: Integral a => a -> a -> Bool
d `divides` n | d == 0    = error "divides: zero divisor"
              | otherwise = n `rem` d == 0

factors :: Integer -> [Integer]
factors n | n < 1     = error "argument is not positive"
          | n == 1    = []
          | otherwise = p : factors (n `div` p) where p = ldp n

primes1 :: [Integer]
primes1 = 2 : filter prime1 [3 .. ]

prime1 :: Integer -> Bool
prime1 n | n < 1     = error "not a positive integer"
         | n == 1    = False
         | otherwise = ldp n == n

ldp :: Integer -> Integer
ldp = ldpf primes1 where
    ldpf :: [Integer] -> Integer -> Integer
    ldpf (p : ps) n | p `divides` n = p
                    | p^2 > n       = n
                    | otherwise     = ldpf ps n

distinctFactors :: Integer -> [Integer]
distinctFactors = nub . factors

factorCount :: Integer -> Int
factorCount = length . distinctFactors

main :: IO ()
main = print $ take 1 $ filter (\x -> factorCount x == 4 && factorCount (x + 1) == 4 && factorCount (x + 2) == 4 && factorCount (x + 3) == 4) [1..]
