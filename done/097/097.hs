module Main (main) where

main :: IO ()
main = print $ (succ (28433 * 2 ^ 7830457)) `rem` 10000000000
