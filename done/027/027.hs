module Main where

import Data.Numbers.Primes (isPrime)
import Data.List (maximumBy)

formula :: Integral a => a -> a -> a -> a
formula a b n = n^2 + a*n + b

consec :: Integral a => a -> a -> [a]
consec a b = map (formula a b) [0 .. ]

consecPrimes :: Integral a => a -> a -> [a]
consecPrimes a b = takeWhile isPrime $ consec a b

consecPrimesLen :: Integral a => a -> a -> Int
consecPrimesLen a b = length $ consecPrimes a b

comboList :: Integral a => [(a, a, Int)]
comboList = [(x, y, z) | x <- [-1000 .. 1000], y <- [-1000 .. 1000], let z = consecPrimesLen x y, z > 39]

maximumConsecPrimes :: Integral a => (a, a, Int)
maximumConsecPrimes = maximumBy compareThd3 comboList

compareThd3 :: Ord c => (a, b, c) -> (a, b, c) -> Ordering
compareThd3 (_, _, z) (_, _, w) = compare z w

main :: IO ()
main = print $ (\ (x, y, _) -> x*y) maximumConsecPrimes
