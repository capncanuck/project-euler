module Main where

isLychrel :: Integer -> Bool
isLychrel = trial 0

trial :: Int -> Integer -> Bool
trial 0 n                     = trial 1 (r n)
trial step n | step == 49     = True
             | isPalindrome n = False
             | otherwise      = trial (succ step) (r n)

r :: Integer -> Integer
r n = n + intReverse n

intReverse :: Integer -> Integer
intReverse n = read $ (reverse . show) n

isPalindrome :: Integer -> Bool
isPalindrome n = n == intReverse n
