/* The prime factors of 13195 are 5, 7, 13 and 29.
 *
 * What is the largest prime factor of the number 600851475143 ?
 */

#include <iostream>
#include <cmath>
using namespace std;

bool isprime(long long n) {
    int start = floor(sqrt(n));
    for (int i = start; i > 1; i--) {
        if (!(n % i)) {
            return false;
        }
    }
    return true;
}

long long largest_factor(long long n) {
    int end = ceil(sqrt(n));

    for (int i = 2; i < end; i++) {
        if (!(n % i)) {
            return n/i;
        }
    }

    return n;
}

int main() {
    long long composite = 600851475143;
    long long factor = largest_factor(composite);

    while (true) {
        if (!(isprime(factor))) {
            factor = largest_factor(factor);
        } else {
            cout << factor << endl;
            break;
        }
    }

    return 0;
}
