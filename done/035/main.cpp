/* The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
 *
 * There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
 *
 * How many circular primes are there below one million?
 */

#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;
typedef unsigned int uint;

// rotates right to left
int rotate(int num, int digits) {
    string s_num;
    stringstream stream;
    int out = 0;

    stream << num;
    s_num = stream.str();

    for (uint i = 1; i < s_num.length(); i++) {
        out += pow(10, s_num.length() - i) * (s_num[i] - 48);
    }

    out += (s_num[0] - 48);

    if (out < pow(10, digits)) {
        out *= pow(10,(digits - floor(log(out)/log(10))) - 1);
    }

    return out;
}

bool prime(int number) {
    if (number == 1) {
        return false;
    } else    {
        int limit = floor(sqrt(number)) + 1;
        int i = 2;

        while (i < limit) {
            if (!(number % i)) {
                return false;
            }

            i ++;
        }

        return true;
    }
}

bool circular(int num) {
    int digits = ceil(log(num)/log(10));

    if (!(prime(num))) {
        return false;
    } else {
        int rotated = rotate(num, digits);

        while (rotated != num) {
            if (!(prime(rotated))) {
                return false;
            }

            rotated = rotate(rotated, digits);
        }

        return true;
    }
}

int main() {

    int count = 0;

    for (int i = 2; i < 1e6; i ++) {
        if (circular(i)) {
            count++;
        }
    }

    cout << count << endl;

    return 0;
}
