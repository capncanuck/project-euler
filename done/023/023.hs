module Main (main) where

import Data.List (nub)

divides :: Integral a => a -> a -> Bool
d `divides` n | d == 0    = error "divides: zero divisor"
              | otherwise = n `rem` d == 0

divisors :: Integer -> [(Integer, Integer)]
divisors n = [(d, n `quot` d) | d <- [1 .. k], d `divides` n] where
    k :: Integer
    k = floor (sqrt (fromInteger n))

divs :: Integer -> [Integer]
divs n = fst list ++ reverse (snd list) where
    list = unzip (divisors n)

properDivs :: Integer -> [Integer]
properDivs n = nub $ init (divs n)

perfect :: Integer -> Bool
perfect n = sum (properDivs n) == n

deficient :: Integer -> Bool
deficient n = sum (properDivs n) < n

abundant :: Integer -> Bool
abundant n = sum (properDivs n) > n

abundants :: Integer -> [Integer]
abundants limit = takeWhile (<= limit) [x | x <- [1..], abundant x]

sum2abundants :: Integer -> [Integer] -> Bool
sum2abundants _ []                       = False
sum2abundants n xs@(x : xs') | n < x     = False
                             | otherwise = (n - x) `elem` xs || sum2abundants n xs'

main :: IO ()
main = print $ sum [x | x <- [1..28123], not (sum2abundants x (abundants x))]
