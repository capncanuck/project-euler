module Main where

import Data.Ratio ((%), Ratio, numerator)
import Data.Char (digitToInt)

continuedFraction :: (RealFrac a, Integral b) => a -> [b]
continuedFraction n = t : continuedFraction (1 / (n - fromIntegral t))
    where
        t = truncate n

convergants :: Integral a => [a] -> [Ratio a]
convergants xs = partial 1 xs
    where
        partial :: Integral a => Int -> [a] -> [Ratio a]
        partial n xs = partial' (take n xs) : partial (n + 1) xs

        partial' :: Integral a => [a] -> Ratio a
        partial' (x : xs) | xs == []  = fromIntegral x
                          | otherwise = fromIntegral x + (1 / partial' xs)

e :: Integral a => [a]
e = 2 : 1 : concat [[k, 1, 1] | k <- [2, 4 .. ]]

sumDigits :: Integral a => a -> Int
sumDigits = sum . map digitToInt . show

main :: IO ()
main = print
     $ let n = 100 in
          sumDigits
        $ numerator
        $ flip (!!) (n - 1)
        $ convergants e
