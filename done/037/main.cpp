/* The number 3797 has an interesting property.
 * Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7.
 * Similarly we can work from right to left: 3797, 379, 37, and 3.
 *
 * Find the sum of the only eleven primes that are both truncatable from left to right and right to left.
 *
 * NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
 */

#include <iostream>
#include <cmath>

using namespace std;

bool prime(int num) {
    if (num == 1) {
        return false;
    } else    {
        int limit = floor(sqrt(num)) + 1;
        int i = 2;

        while (i < limit) {
            if (!(num % i)) {
                return false;
            }

            i ++;
        }

        return true;
    }
}

int highest_placevalue(int num) {
    return pow(10, floor(log(num) / log(10)));
}

int truncate(int num, string direction) {
    if (direction == "left") {
        return num %= (int) (floor(num / highest_placevalue(num)) * highest_placevalue(num));
    } else if (direction == "right") {
        return floor(num / 10);
    } else {
        return -1;
    }
}

bool truncatable_prime(int num, string direction) {
    if (!(num)) {
        return true;
    } else if (!(prime(num))) {
        return false;
    } else {
        return (truncatable_prime(truncate(num, direction), direction));
    }
}

int main() {
    int sum = 0;
    int count = 0;
    int check = 10; // From the problem, primes < 10 are not considered to be truncatable primes.

    while (count < 11) {
        if (truncatable_prime(check, "left") && truncatable_prime(check, "right")) {
            count++;
            sum += check;
        }
        check++;
    }

    cout << sum << endl;

    return 0;
}
