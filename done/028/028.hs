module Main where

main :: IO ()
main =
    print $
        sum $
            init $
                let m = 1001 in
                    [x^2 + (x + 1) `rem` 2 | x <- [1 .. m]] ++
                    [x^2 +  x + 1          | x <- [1 .. m]]
