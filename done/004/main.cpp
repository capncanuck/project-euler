/* A palindromic number reads the same both ways.
 * The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 *
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */

#include <iostream>
#include <cmath>
using namespace std;

bool ispalindome(int n) {
    int initial = n;
    int digit;
    int reverse = 0;

    while (n > 0) {
        digit = n % 10;
        reverse = reverse * 10 + digit;
        n = floor(n/10);
    }

    if (initial == reverse) {
        return true;
    } else {
        return false;
    }
}

int main() {
    int product = 999 * 999;
    int j;
    bool done = false;

    while (!done) {
        if (ispalindome(product)) {
            for (int i = 999; i > 99; i--) {
                if (!(product % i)) {
                    j = product / i;
                    if (j > 99 && j < 1000) {
                        cout << i << " * " << j << " = " << product << endl;
                        done = true;
                        break;
                    }
                }
            }
        }

        product--;
    }

    return 0;
}
