module Main where

import Data.List.Split (splitOn)
import Data.List (maximumBy)

main :: IO Integer
main = do
    s <- readFile "base_exp.txt"
    return
        $ fst
        $ maximumBy compareSnd
        $ addLineNo
        $ parseData s

parseData :: String -> [[Integer]]
parseData s = parseInt $ (map (splitOn ",") . lines) s
    where
        parseInt :: [[String]] -> [[Integer]]
        parseInt = map (map str2Integer)

        str2Integer :: String -> Integer
        str2Integer = read

addLineNo :: Integral a => [b] -> [(a, b)]
addLineNo = addLineNo' 1
    where
        addLineNo' :: Integral a => a -> [b] -> [(a, b)]
        addLineNo' _ []       = []
        addLineNo' n (x : xs) = (n, x) : addLineNo' (succ n) xs

compareSnd :: Integral b => (a, [b]) -> (a, [b]) -> Ordering
compareSnd (_, x) (_, y) = compareExp x y
    where
        compareExp :: Integral a => [a] -> [a] -> Ordering
        compareExp [x1, x2] [y1, y2] = compare (a2 * log a1) (b2 * log b1)
            where
                a1 = fromIntegral x1
                a2 = fromIntegral x2

                b1 = fromIntegral y1
                b2 = fromIntegral y2
