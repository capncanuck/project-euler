module Main (main) where

type Part      = [Int]
type CmprzPart = (Int, Part)

expand :: CmprzPart -> Part
expand (0, p) = p
expand (n, p) = 1 : expand (n - 1, p)

nextPartition :: CmprzPart -> CmprzPart
nextPartition (k, x : xs) = pack (x - 1) (k + x, xs) where
    pack :: Int -> CmprzPart -> CmprzPart
    pack 1 (m, xs) = (m, xs)
    pack k (m, xs) | k > m     = pack (k - 1) (m, xs)
                   | otherwise = pack k (m - k, k : xs)

generatePs :: CmprzPart -> [Part]
generatePs p@(n, [])       = [expand p]
generatePs p@(n, x : xs)   = expand p : generatePs (nextPartition p)

part :: Int -> [Part]
part n | n < 1     = error "part: argument < 1"
       | n == 1    = [[1]]
       | otherwise = generatePs (0, [n])

main :: IO ()
main = print $ length (part 100) - 1
