module Main where

main :: IO ()
main = (print . sum . filter (\i->i `mod` 3 == 0 || i `mod` 5 == 0)) [3 .. 999]
