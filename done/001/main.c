/* Project Euler Problem 1
 * Add all the natural numbers below one thousand that are multiples of 3 or 5.
 * http://projecteuler.net/index.php?section=problems&id=1
 *
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
 * get 3, 5, 6 and 9. The sum of these multiples is 23.
 *
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */
#include <stdio.h>

typedef unsigned int u32;

int main(void) {
    u32 sum = 0;

    /* Get the sum of all multiples of 3  or 5 */
    u32 i = 3;
    while (i < 1000) {
        if ((i % 3 == 0) || (i % 5 == 0)) {
            sum += i;
        }
        ++i;
    }

    printf("Sum of all multiples of 3 or 5 below 1000 is: %u\n", sum);

    return 0;
}
