module Main (main) where

import Data.Char (digitToInt)

fac :: Integer -> Integer
fac 0 = 1
fac n = n * fac (n - 1)

main :: IO ()
main = print $ sum $ map digitToInt $ show $ fac 100
