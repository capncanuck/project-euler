/* A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * a^2 + b^2 = c^2
 *
 * For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
 *
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */

#include <iostream>
#include <cmath>
using namespace std;

bool issquare(int n) {
    if (sqrt(n) == floor(sqrt(n))) {
        return true;
    } else {
        return false;
    }
}

int main() {
    int a = 1;
    int b = 1;
    int c;

    while (true) {
        if (issquare(pow(a,2)+ pow(b,2))) {
            c = sqrt(pow(a,2)+ pow(b,2));
            if (a + b + c == 1000) {
                break;
            }
        }

        if (a < 1000) {
            a++;
        } else {
            a = 1;
            b++;
        }
    }

    cout << a << " + " << b << " + " << c << " = " << 1000 << endl;
    cout << a << " * " << b << " * " << c << " = " << a * b * c << endl;

    return 0;
}
