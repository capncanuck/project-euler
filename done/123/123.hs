module Main (main) where

import Data.Numbers.Primes (primes)

divides :: Integral a => a -> a -> Bool
d `divides` n | d == 0    = error "divides: zero divisor"
              | otherwise = n `rem` d == 0

p :: Int -> Integer
p n = primes !! (n - 1)

r :: Int -> Integer
r n = ((p n - 1)^n + (p n + 1)^n) `rem` (p n ^ 2)

main :: IO ()
main = print $ head [n | n <- [1 .. ], r n > 10^10]
