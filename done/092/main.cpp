/**
 * A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.
 *
 * For example,
 *
 * 44 → 32 → 13 → 10 → 1 → 1
 * 85 → 89 → 145 → 42 → 20 → 4 → 16 → 37 → 58 → 89
 *
 * Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop.
 * What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.
 *
 * How many starting numbers below ten million will arrive at 89?
 *
 */

#include <iostream>
#include <cmath>

using namespace std;

int sum_square_digits(int num) {
    int sum = 0;
    while (num > 0) {
        sum += pow((num % 10),2);
        num /= 10;
    }
    return sum;
}

int main() {
    int count = 0;
    int n;

    for (int i = 1; i < 1e7; i++) {
        n = i;
        while (n != 1 && n != 89) {
            n = sum_square_digits(n);
        }
        if (n == 89) {
            count++;
        }
    }

    cout << count << endl;
    return 0;
}
