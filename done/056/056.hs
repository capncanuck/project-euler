module Main where

digitalSum :: Integer -> Integer
digitalSum 0 = 0
digitalSum n = n `rem` 10 + digitalSum (n `div` 10)

main :: IO ()
main = print $ maximum [digitalSum (a^b) | a <- [0 .. 99], b <- [0..99]]
