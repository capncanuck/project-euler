module Main where

import Data.Foldable (foldr')

foldr1' :: (a -> a -> a) -> [a] -> a
foldr1' _ [] = error "foldr1': empty list"
foldr1' f xs = foldr' f (last xs) (init xs)

maximumSum :: [[Int]] -> Int
maximumSum = head . foldr1' reduce

reduce :: [Int] -> [Int] -> [Int]
reduce a b = zipWith (+) a (zipWith max b (tail b))

main :: IO ()
main = readFile "triangle.txt" >>= print . maximumSum . map (map read . words) . lines
