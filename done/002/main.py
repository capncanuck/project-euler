#! /usr/bin/python

def fibonacci(n):
    if n == 0 or n == 1:
        return n + 1
    else:
        return fibonacci(n - 2) + fibonacci(n - 1)

i = 0
sum = 0
current = fibonacci(i)

while current < 4000000:
    if not (current % 2):
        sum += current
    i += 1
    current = fibonacci(i)

print sum 
