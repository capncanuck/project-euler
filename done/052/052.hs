module Main (main) where

import Data.List (sort, nub)

main :: IO ()
main = print $ head [x | x <- [1 ..], condition x]

sortDigits :: Integer -> String
sortDigits = sort . show

condition :: Integer -> Bool
condition x = listOfEquals $ map (sortDigits . (x *)) [2 .. 6]

listOfEquals :: Eq a => [a] -> Bool
listOfEquals = (==) 1 . length . nub
