module Main where

import Data.List (find)
import Data.Maybe (fromJust)

fibgen :: [(Integer, Integer)]
fibgen = (1, 1) : (2, 1) : next fibgen where
    next ((n, a) : t@((_, b) : _)) = (n + 2, a + b) : next t

digitCount :: Integer -> Int
digitCount = length . show

testLimit :: (Integer, Integer) -> Bool
testLimit (_, y) = digitCount y == 1000

main :: IO ()
main = print
     $ fst
     $ fromJust
     $ find testLimit fibgen
