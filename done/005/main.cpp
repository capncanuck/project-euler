/* 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 *
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */

#include <iostream>
using namespace std;

const int LIMIT = 20;

int gcd(int a, int b) {
    if (b == 0) {
        return a;
    } else {
        return gcd(b, a % b);
    }
}

int lcm(int* a) {
    int product = 1;
    int size = LIMIT;

    for (int i = 0; i < size; i++) {
        product *= a[i]/gcd(product, a[i]);
    }

    return product;
}

int main() {
    int array[LIMIT];

    for (int i = 0; i < LIMIT; i++) {
        array[i] = i + 1;
    }

    cout << lcm(array) << endl;

    return 0;
}

