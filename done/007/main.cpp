/* By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
 * we can see that the 6th prime is 13.
 * What is the 10001st prime number?
 */

#include <iostream>
#include <cmath>
using namespace std;

bool isprime(int n) {
    int start = floor(sqrt(n));

    for (int i = start; i > 1; i--) {
        if (!(n % i)) {
            return false;
        }
    }

    return true;
}

int main() {

    int i = 0;
    int j = 0;

    while (j < 10002) {
        i++;
        if (isprime(i)) {
            j++;
        }
    }

    cout << i << endl;

    return 0;
}
