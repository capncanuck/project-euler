module Main where

import Data.Function (on)
import Data.Ratio ((%), Ratio, denominator)

cancel :: Int -> Int -> Ratio Int -> Bool
cancel xs ys xy | v == '0'  = False
                | y == u    = ((%) `on` read) [x] [v] == xy
                | otherwise = False
    where
        [x, y] = show xs
        [u, v] = show ys

main :: IO ()
main = (print . denominator . product) [xy |
    x <- [12 .. 98],
    y <- [(x + 1) .. 98],
    let xy = x % y,
    cancel x y xy]
