module Main where

import Data.Ratio ((%), numerator)

choose :: Integral a => a -> a -> a
choose n k = numerator $ product [succ k .. n] % product [1 .. n - k]

paths :: Integral a => a -> a
paths n = choose (2 * n) n

main :: IO ()
main = print $ paths 20
