module Main where

import Ratio ((%), Rational, numerator, denominator)
import Data.Char (digitToInt)

continuedFraction :: RealFrac a => a -> [Integer]
continuedFraction n = t : continuedFraction (1 / (n - (fromIntegral $ t)))
    where
        t = truncate n

convergants :: [Integer] -> [Rational]
convergants xs = partial 1 xs
    where
        partial :: Int -> [Integer] -> [Rational]
        partial n xs = partial' (take n xs) : partial (succ n) xs

        partial' :: [Integer] -> Rational
        partial' (x : xs) | xs == []  = (fromIntegral x)
                          | otherwise = (fromIntegral x) + (1 / (partial' xs))

higherNumThenDen :: Rational -> Bool
higherNumThenDen r = ((digitCount . numerator) r) > ((digitCount . denominator) r)

digitCount :: Integer -> Int
digitCount = (length . show)

main :: IO ()
main = print
     $ length
     $ filter (higherNumThenDen)
     $ take 1001
     $ convergants
     $ continuedFraction (sqrt 2)
