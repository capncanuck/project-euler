module Main where

import Data.List (delete)
import Data.List.Utils (genericJoin)


permutation :: Eq a => [a] -> [[a]]
permutation [] = [[]]
permutation xs = [x : ys | x <- xs, ys <- permutation (delete x xs)]

main :: IO ()
main = (putStrLn . genericJoin "") $ permutation [0 .. 9] !! 999999
