/* Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names,
 * sort it into alphabetical order.
 * Then working out the alphabetical value for each name,
 * multiply this value by its alphabetical position in the list to obtain a name score.
 *
 * For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53,
 * is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.
 *
 * What is the total of all the name scores in the file?
 *
 * NB:
 *         - I formatted names.txt to be easier to work with.
 *         - names.txt contains 5163 names
 */


#include <iostream>
#include <fstream>
using namespace std;
typedef unsigned int uint;

int word_score(string word) {
    int score = 0;

    for (uint i = 0; i < word.length(); i++) {
        score += word[i] - 64;
    }

    return score;
}

int main() {
    ifstream names("names.txt");
    string list[5163];
    int sum = 0;

    for (int i = 0; i < 5163; i++) {
        getline(names, list[i]);
        sum += (i + 1) * word_score(list[i]);
    }

    cout << sum << endl;

    return 0;
}
