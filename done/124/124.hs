module Main where

import Data.Numbers.Primes (primeFactors)
import Data.List (nub, sort)

rad :: Integer -> Integer
rad = product . nub . primeFactors

sorted :: Integer -> [(Integer, Integer)]
sorted n = sort [(rad x, x) | x <- [1 .. n]]

e :: Integer -> Int -> Integer
e n k = snd $ sorted n !! (k - 1)

main :: IO ()
main = print
     $ e 100000 10000
