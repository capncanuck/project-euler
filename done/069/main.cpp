/* Euler's Totient function, φ(n) [sometimes called the phi function],
 * is used to determine the number of numbers less than n which are relatively prime to n.
 * For example, as 1, 2, 4, 5, 7, and 8, are all less than nine and relatively prime to nine, φ(9)=6.
 *
 *     n     Relatively Prime    φ(n)    n/φ(n)
 *     2     1                    1        2
 *     3     1,2                    2        1.5
 *     4     1,3                    2        2
 *     5     1,2,3,4                4        1.25
 *     6     1,5                    2        3
 *     7     1,2,3,4,5,6            6        1.1666...
 *     8     1,3,5,7                4        2
 *     9     1,2,4,5,7,8            6        1.5
 *     10     1,3,7,9                4        2.5
 *
 * It can be seen that n=6 produces a maximum n/φ(n) for n ≤ 10.
 *
 * Find the value of n ≤ 1,000,000 for which n/φ(n) is a maximum.
 */

#include <iostream>
#include <cmath>

using namespace std;

int gcd(int a, int b) {
    if (b == 0) {
        return a;
    } else {
        return gcd(b, a % b);
    }
}

double totient_quotient(int n) {
    int count = 1;

    if (!(n % 2)) {
        for (int i = 2; i < n; i += 2) {
            if (gcd(n, i + 1) == 1) {
                count++;
            }
        }
    } else if (!(n % 10)) {
        return 4;
    } else if (log(n)/log(6) == (int) log(n)/log(6)) {
        return 3;
    } else if (log(n)/log(2) == (int) log(n)/log(2)) {
        return 2;
    } else if (log(n)/log(3) == (int) log(n)/log(3)) {
        return 1.5;
    } else if (log(n)/log(5) == (int) log(n)/log(5)) {
        return 1.25;
    } else if (log(n)/log(7) == (int) log(n)/log(7)) {
        return 7/6;
    } else if (log(n)/log(11) == (int) log(n)/log(11)) {
        return 1.1;
    } else {
        for (int i = 1; i < n; i++) {
            if (gcd(n, i + 1) == 1) {
                count++;
            }
        }
    }

    return (double) n/count;
}

int main() {
    double maximum = 2;
    double quotient = 2;
    int n = 2;

    for (int i = 2; i < 1000001; i++) {
        quotient = totient_quotient(i);

        if (!(i % 10000)) {
            cout << i << endl;
        }

        if (quotient > maximum) {
            maximum = quotient;
            n = i;
        }
    }

    cout << n << ": " << maximum << endl;

    return 0;
}
