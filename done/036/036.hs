module Main where

import Data.Char (intToDigit)
import Numeric (showIntAtBase)

palindrome :: Eq a => [a] -> Bool
palindrome list = list == reverse list

toBinary :: Integer -> String
toBinary n = showIntAtBase 2 intToDigit n ""

main :: IO ()
main = print $ sum [n | n <- enumFromThenTo 1 3 999999, (palindrome . show) n, (palindrome . toBinary) n]
