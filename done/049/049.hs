module Main where

import Data.Numbers.Primes (isPrime)
import Data.List ((\\), nub)
import CombinatoricsGeneration (combinationsOf)

primes4d :: [Integer]
primes4d = [x | x <- [1000 .. 9999], isPrime x]

isPerm :: Eq a => [a] -> [a] -> Bool
isPerm xs ys | length xs /= length ys = False
             | otherwise = isPerm' xs ys
    where
        isPerm' :: Eq a => [a] -> [a] -> Bool
        isPerm' [] [] = True
        isPerm' [] x  = False
        isPerm' (x : xs) ys | x `elem` ys = isPerm xs (ys \\ [x])
                            | otherwise   = False

findAllPerm :: Eq a => [a] -> [[a]] -> [[a]]
findAllPerm x = filter (isPerm x)

permSet :: Eq a => [[a]] -> [[[a]]]
permSet []         = []
permSet xs@(x : _) = set : permSet (xs \\ set)
    where
        set = findAllPerm x xs

filterLength :: Eq a => [[a]] -> Int -> [[a]]
filterLength xs n = filter (\ x -> length x >= n) xs

main :: IO ()
main = print
     $ concat
     $ flip (!!) 1
     $ filter filterSeq
     $ concatMap (combinationsOf 3)
     $ flip filterLength 3
     $ permSet
     $ map show primes4d
    where
        filterSeq :: [String] -> Bool
        filterSeq = isArithSeq . map  read

        isArithSeq :: Integral a => [a] -> Bool
        isArithSeq xs = 2 > length (nub $ diff xs)

        diff :: Integral a => [a] -> [a]
        diff []                 = []
        diff [x]                = [x]
        diff [x, y]             = [y - x]
        diff (x : rest@(y : _)) = diff [x, y] ++ diff rest
