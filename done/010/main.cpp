/* The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 *
 * Find the sum of all the primes below two million.
 */

#include <iostream>
#include <cmath>
using namespace std;

bool isprime(int n) {
    int start = floor(sqrt(n));
    for (int i = start; i > 1; i--) {
        if (!(n % i)) {
            return false;
        }
    }
    return true;
}

int main() {
    long long sum = 0;
    for (int j = 2; j < 2000000; j++) {
        if (isprime(j)) {
            sum += j;
        }
    }

    cout << sum << endl;

    return 0;
}
