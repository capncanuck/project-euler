/* The sum of the squares of the first ten natural numbers is,
 * 1^2 + 2^2 + ... + 10^2 = 385
 *
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)^2 = 55^2 = 3025
 *
 * Hence the difference between the sum of the squares of the first ten natural numbers
 * and the square of the sum is 3025 − 385 = 2640.
 *
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */

#include <iostream>
#include <cmath>
using namespace std;

int main() {

    int limit = 100;
    int sum[] = {
        0,
        0
    };

    for (int i = 1; i < limit + 1; i ++) {
        sum[0] += pow(i, 2);
    }

    for (int j = 1; j < limit + 1; j ++) {
        sum[1] += j;
    }

    cout << (long long) pow(sum[1], 2) - sum[0] << endl;


    return 0;
}
